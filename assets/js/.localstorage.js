
function() {
    html.hasClass("dark-mode") ? (html.removeClass("dark-mode"), localStorage.setItem("alto_dark", !1)) : (html.addClass("dark-mode"), localStorage.setItem("alto_dark", !0))
  }